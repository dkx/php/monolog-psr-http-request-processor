# DKX/PHP/MonologPsrHttpRequestProcessor

Monolog processor for PSR Http requests

## Installation

```bash
$ composer require dkx/monolog-psr-http-request-processor
```

## Usage

```php
<?php

use DKX\MonologPsrHttpRequestProcessor\HttpRequestProcessor;
use DKX\MonologPsrHttpRequestProcessor\HttpRequestProviderInterface;
use Monolog\Logger;
use Psr\Http\Message\ServerRequestInterface;

$requestProvider = new class implements HttpRequestProviderInterface
{
    public function getRequest() : ?ServerRequestInterface
    {
        return get_current_http_request_somehow();
    }
};

$logger = new Logger('default');
$logger->pushProcessor(new HttpRequestProcessor($requestProvider));
```
