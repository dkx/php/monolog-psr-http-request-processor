<?php

declare(strict_types=1);

namespace DKX\MonologPsrHttpRequestProcessorTests\Tests;

use DKX\MonologPsrHttpRequestProcessor\HttpRequestProcessor;
use DKX\MonologPsrHttpRequestProcessor\HttpRequestProviderInterface;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\UriInterface;

final class HttpRequestProcessorTest extends TestCase
{
	public function tearDown(): void
	{
		parent::tearDown();
		\Mockery::close();
	}

	public function testHandle_withoutRequest(): void
	{
		/** @var \DKX\MonologPsrHttpRequestProcessor\HttpRequestProviderInterface|\Mockery\MockInterface $requestProvider */
		$requestProvider = \Mockery::mock(HttpRequestProviderInterface::class)
			->shouldReceive('getRequest')->andReturnNull()->getMock();

		$processor = new HttpRequestProcessor($requestProvider);
		$record = $processor([]);

		self::assertEquals([], $record);
	}

	public function testHandle(): void
	{
		/** @var \Psr\Http\Message\UriInterface|\Mockery\MockInterface $uri */
		$uri = \Mockery::mock(UriInterface::class)
			->shouldReceive('__toString')->andReturn('http://localhost:8080')->getMock();

		/** @var \Psr\Http\Message\ServerRequestInterface|\Mockery\MockInterface $request */
		$request = \Mockery::mock(ServerRequestInterface::class)
			->shouldReceive('getMethod')->andReturn('GET')->getMock()
			->shouldReceive('getProtocolVersion')->andReturn('1.1')->getMock()
			->shouldReceive('getUri')->andReturn($uri)->getMock()
			->shouldReceive('getHeaders')->andReturn([
				'Content-type' => ['application/json'],
				'X-Test' => ['1', '2', '3'],
			])->getMock();

		/** @var \DKX\MonologPsrHttpRequestProcessor\HttpRequestProviderInterface|\Mockery\MockInterface $requestProvider */
		$requestProvider = \Mockery::mock(HttpRequestProviderInterface::class)
			->shouldReceive('getRequest')->andReturn($request)->getMock();

		$processor = new HttpRequestProcessor($requestProvider);
		$record = $processor([]);

		self::assertEquals([
			'extra' => [
				'http_request' => [
					'method' => 'GET',
					'protocol' => '1.1',
					'uri' => 'http://localhost:8080',
					'headers' => [
						'content-type' => 'application/json',
						'x-test' => ['1', '2', '3'],
					],
				],
			],
		], $record);
	}
}
