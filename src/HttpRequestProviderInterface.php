<?php

declare(strict_types=1);

namespace DKX\MonologPsrHttpRequestProcessor;

use Psr\Http\Message\ServerRequestInterface;

interface HttpRequestProviderInterface
{
	public function getRequest(): ?ServerRequestInterface;
}
