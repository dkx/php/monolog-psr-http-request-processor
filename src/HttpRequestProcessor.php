<?php

declare(strict_types=1);

namespace DKX\MonologPsrHttpRequestProcessor;

use Monolog\Processor\ProcessorInterface;
use Nette\Utils\Strings;

final class HttpRequestProcessor implements ProcessorInterface
{
	/** @var \DKX\MonologPsrHttpRequestProcessor\HttpRequestProviderInterface */
	private $requestProvider;

	public function __construct(HttpRequestProviderInterface $requestProvider)
	{
		$this->requestProvider = $requestProvider;
	}

	public function __invoke(array $record)
	{
		$request = $this->requestProvider->getRequest();

		if ($request === null) {
			return $record;
		}

		if (!\array_key_exists('extra', $record)) {
			$record['extra'] = [];
		}

		$record['extra']['http_request'] = [
			'method' => $request->getMethod(),
			'protocol' => $request->getProtocolVersion(),
			'uri' => (string) $request->getUri(),
			'headers' => $this->processHeaders($request->getHeaders()),
		];

		return $record;
	}

	private function processHeaders(array $headers): array
	{
		$result = [];
		foreach ($headers as $key => $value) {
			$result[Strings::lower($key)] = \count($value) === 1 ? \reset($value) : $value;
		}

		return $result;
	}
}
